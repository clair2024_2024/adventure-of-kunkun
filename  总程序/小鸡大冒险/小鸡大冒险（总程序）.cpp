﻿#include <stdio.h>
#include <graphics.h>	
#include <conio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include "resource.h"
#pragma comment(lib, "Winmm.lib") 
#define PI 3.14159265
/*  Sounds list:
IDR_WAVE1 : fire_sound
IDR_WAVE2 : zombie_get_hit_1
IDR_WAVE3 : zombie_get_hit_2
IDR_WAVE4 : cxk_get_hit_1
IDR_WAVE5 : get_item
*/
int inventory(int wp[], int* open_inventory, int* ix, int* iy, int* strength, int* quickness, int* intelligence, int* lv, bool* itenchange);//物品栏
int item(int x, int y, int dsx, int dsy, int id); //物品栏物品显示
int stage_1_mv();
int stage2(double* x, double* y, int* gain_key);
double x = 520, y = 390;
double kwidth;
double khight;
bool FullScreen=false;
bool sizechange = true;
bool doorchange = true;
bool itemchange = true;
HDC MainHdc;
HWND hwnd;
RECT rect;
IMAGE kk[4];
IMAGE img;
WINDOWPLACEMENT wp;
IMAGE GEN[7];
IMAGE door3;
IMAGE jch;
IMAGE ball[4]; // 篮球 的贴图 
IMAGE bk;  //   򳡱篮球场 的 贴图
IMAGE bk2;//       小径 的 贴图
IMAGE bk3;//       小径 的 贴图
IMAGE zb[4];  //僵尸 的 贴图
IMAGE pause[2];  //暂停界面 的 贴图
IMAGE key[2];  //钥匙 的 贴图
IMAGE ys[2];  //原石 的 贴图
IMAGE hen[2]; //鸡的贴图
IMAGE kk1[4];
IMAGE die_1; //死亡的贴图
IMAGE kuwu[2]; //苦无的贴图
IMAGE dragon[4]; //龙的贴图
IMAGE fb[2]; //火球的贴图
IMAGE wb[2]; //水球的贴图
IMAGE arrow[8]; //箭头的贴图
void SwordWeaponAttact(int xposition,int x, int y,IMAGE weapon[7], IMAGE rotate_weapon[2],int* rotate_count , int* able_to_attack ,double SwordRotateAngel) {

	

		   *rotate_count += 1 ;

		   if ( *able_to_attack == 1 )
		   {

		   
			  if (xposition == 'a')//判断kk的移动方向
			  {

				  rotateimage(&rotate_weapon[0], &weapon[0], SwordRotateAngel * ( (*rotate_count)/20), WHITE, false, true);
				  rotateimage(&rotate_weapon[1], &weapon[1], SwordRotateAngel * ( (*rotate_count)/20), BLACK, false, true);


				  if (*rotate_count <= 20) 
				  {
					  putimage(x - 30, y - 60, &rotate_weapon[0], SRCAND);  putimage(x - 30, y - 60, &rotate_weapon[1], SRCINVERT);
				  }
					

				  if (*rotate_count > 20 && *rotate_count <= 40) 
				  {
					  putimage(x - 45, y - 55, &rotate_weapon[0], SRCAND);  putimage(x - 45, y - 55, &rotate_weapon[1], SRCINVERT);
				  }
					  

				  if (*rotate_count > 40 && *rotate_count <= 60) 
				  {
					  putimage(x - 60, y - 40, &rotate_weapon[0], SRCAND);  putimage(x - 60, y - 40, &rotate_weapon[1], SRCINVERT);
				  }

				  if (*rotate_count > 60 && *rotate_count <= 80) 
				  {
					  putimage(x - 70, y - 30, &rotate_weapon[0], SRCAND);  putimage(x - 70, y - 30, &rotate_weapon[1], SRCINVERT);
				  }
					  

				  if (*rotate_count > 80 && *rotate_count <= 100) 
				  {
					  putimage(x - 70, y - 20, &rotate_weapon[0], SRCAND);  putimage(x - 70, y - 20, &rotate_weapon[1], SRCINVERT);
				  }
					  

				  if (*rotate_count > 100 && *rotate_count <= 120) 
				  {
					  putimage(x - 70, y - 5, &rotate_weapon[0], SRCAND);  putimage(x - 70, y - 5, &rotate_weapon[1], SRCINVERT);
				  }
					  

				  if (*rotate_count > 120 && *rotate_count <= 140) 
				  {
					  putimage(x - 70, y + 10, &rotate_weapon[0], SRCAND);  putimage(x - 70, y + 10, &rotate_weapon[1], SRCINVERT);
				  }
					  

				  if (*rotate_count > 140 && *rotate_count <= 160) {
					  putimage(x - 65, y + 20, &rotate_weapon[0], SRCAND);  putimage(x - 65, y + 20, &rotate_weapon[1], SRCINVERT);
				  }
					  

				  if (*rotate_count > 160 && *rotate_count < 200)
				  {
					  putimage(x - 60, y + 30, &rotate_weapon[0], SRCAND);  putimage(x - 60, y + 30, &rotate_weapon[1], SRCINVERT);
				  }
				  if (*rotate_count == 200) 
				  {
					  *able_to_attack = 0; *rotate_count = 0;
				  }
					  
			
			  }

			  if (xposition == 'd')//判断kk的移动方向
			  {

				  rotateimage(&rotate_weapon[0], &weapon[0], SwordRotateAngel * (-(*rotate_count) / 20), WHITE, false, true);
				  rotateimage(&rotate_weapon[1], &weapon[1], SwordRotateAngel * (-(*rotate_count) / 20), BLACK, false, true);


				  if (*rotate_count <= 20)
				  {
					  putimage(x + 30, y - 60, &rotate_weapon[0], SRCAND);  putimage(x + 30, y - 60, &rotate_weapon[1], SRCINVERT);
				  }


				  if (*rotate_count > 20 && *rotate_count <= 40)
				  {
					  putimage(x + 45, y - 55, &rotate_weapon[0], SRCAND);  putimage(x + 45, y - 55, &rotate_weapon[1], SRCINVERT);
				  }


				  if (*rotate_count > 40 && *rotate_count <= 60)
				  {
					  putimage(x + 60, y - 40, &rotate_weapon[0], SRCAND);  putimage(x + 60, y - 40, &rotate_weapon[1], SRCINVERT);
				  }

				  if (*rotate_count > 60 && *rotate_count <= 80)
				  {
					  putimage(x + 70, y - 30, &rotate_weapon[0], SRCAND);  putimage(x + 70, y - 30, &rotate_weapon[1], SRCINVERT);
				  }


				  if (*rotate_count > 80 && *rotate_count <= 100)
				  {
					  putimage(x + 70, y - 20, &rotate_weapon[0], SRCAND);  putimage(x + 70, y - 20, &rotate_weapon[1], SRCINVERT);
				  }


				  if (*rotate_count > 100 && *rotate_count <= 120)
				  {
					  putimage(x + 70, y - 5, &rotate_weapon[0], SRCAND);  putimage(x + 70, y - 5, &rotate_weapon[1], SRCINVERT);
				  }


				  if (*rotate_count > 120 && *rotate_count <= 140)
				  {
					  putimage(x + 70, y + 10, &rotate_weapon[0], SRCAND);  putimage(x + 70, y + 10, &rotate_weapon[1], SRCINVERT);
				  }


				  if (*rotate_count > 140 && *rotate_count <= 160) {
					  putimage(x + 65, y + 20, &rotate_weapon[0], SRCAND);  putimage(x + 65, y + 20, &rotate_weapon[1], SRCINVERT);
				  }


				  if (*rotate_count > 160 && *rotate_count < 200)
				  {
					  putimage(x + 60, y + 30, &rotate_weapon[0], SRCAND);  putimage(x + 60, y + 30, &rotate_weapon[1], SRCINVERT);
				  }
				  if (*rotate_count == 200)
				  {
					  *able_to_attack = 0; *rotate_count = 0;
				  }


			  }
		   }

}   

void SwordWeaponAttactDamage(int* xz , int* yz , int*x , int*y , int* zbhp){


}


void BeginPaint() {
	HDC CopyHdc = GetDC(hwnd);
	HDC CopyHwnd= CreateCompatibleDC(CopyHdc);
	HBITMAP CopyHbitmap= CreateCompatibleBitmap(CopyHdc, rect.right, rect.bottom);
	SelectObject(CopyHwnd, CopyHbitmap);
	MainHdc = CopyHwnd;
}
void FlashPaint() {
	HDC mostHDC = GetDC(hwnd);
	BitBlt(mostHDC, 0, 0, rect.right, rect.bottom, MainHdc, 0, 0, SRCCOPY);
	MainHdc = mostHDC;
}
void Putimage(int putx, int puty, int width, int hight, IMAGE *PutImg, DWORD dwRop = SRCCOPY)
{

	HDC ImageHdc = GetImageHDC(PutImg);
	HDC ImageHdcMem = CreateCompatibleDC(ImageHdc);
	HBITMAP ImageBitMap = CreateCompatibleBitmap(ImageHdc, PutImg->getwidth(), PutImg->getheight());
	SelectObject(ImageHdcMem, ImageBitMap);
	BitBlt(ImageHdcMem, 0, 0, PutImg->getwidth(), PutImg->getheight(), ImageHdc, 0, 0, SRCCOPY	);
	SetStretchBltMode(MainHdc, HALFTONE);
	StretchBlt(MainHdc, putx, puty, width, hight, ImageHdcMem, 0, 0, PutImg->getwidth(), PutImg->getheight(), dwRop);
}
DWORD WINAPI DrawCharacterThread(LPVOID lpParam) {
	
	
		Putimage(x * kwidth, (y - 20) * khight, kwidth * GEN[4].getwidth(), GEN[4].getheight() * khight, &GEN[4], SRCAND);
		Putimage(x * kwidth, (y - 20) * khight, kwidth * GEN[5].getwidth(), GEN[5].getheight() * khight, &GEN[5], SRCINVERT);
		Putimage(x * kwidth, y * khight, kwidth * kk[2].getwidth(), kk[2].getheight() * khight, &kk[2], SRCAND);//加载kk的移动方向贴图
		Putimage(x * kwidth, y * khight, kwidth * kk[3].getwidth(), kk[3].getheight() * khight, &kk[3], SRCINVERT);
	
	return 0;
}
void ToggleFullScreen()
{
	int screenWidth = GetSystemMetrics(SM_CXSCREEN);
	int screenHeight = GetSystemMetrics(SM_CYSCREEN);
	if (!FullScreen)
	{

		wp.length = sizeof(WINDOWPLACEMENT);
		if (GetWindowPlacement(hwnd, &wp))
		{
		}
		SetWindowLong(hwnd, GWL_STYLE, WS_POPUP | WS_VISIBLE);
		SetWindowPos(hwnd, HWND_TOP, 0, 0, screenWidth, screenHeight, SWP_NOZORDER);
	}
	else
	{
		SetWindowLong(hwnd, GWL_STYLE, WS_OVERLAPPEDWINDOW | WS_VISIBLE);
		SetWindowPlacement(hwnd, &wp);
	}
	FullScreen = !FullScreen;
}
void keyset(int* zbhp, int* gain_key, double* xz, double* yz, IMAGE* key, double* x, double* y, int* wpn, int wp[], int* flash_wpn)//钥匙逻辑
{

	if (*zbhp <= 0 && *gain_key == 0)//僵尸死后掉落钥匙
	{
		putimage(*xz, *yz, &key[0], SRCAND);
		putimage(*xz, *yz, &key[1], SRCINVERT);
	}
	if (fabs(*x - *xz) < 50 && fabs(*y - *yz) < 50 && *gain_key == 0 && *zbhp <= 0)//钥匙拾取判定范围
	{
		PlaySound(MAKEINTRESOURCE(IDR_WAVE5), NULL, SND_RESOURCE | SND_ASYNC | SND_NOSTOP);
		*gain_key = 1;
		if (*wpn < 45) {   //判断物品栏格子  
			wp[*wpn] = 1;  //1 为钥匙 ID
		}
		*flash_wpn = 1;

	}
}
double zblc(double* xz, double* yz, double* x, double* y, int* hp, double* zspeed, int* hhp, int* zbhp, int* door)//僵尸逻辑
{

	int a;//修复模型坐标偏移的问题
	if (*door == 3)
		a = 90;
	else
		a = 30;
	int b;
	if (*door == 3)
		b = 150;
	else
		b = 0;
	if (*zbhp > 0)//判断僵尸血量不为0
	{
		if (*x - *xz < 0)//判断僵尸和kk的位置进行x轴位移操作
		{
			*xz = (*xz - *zspeed) - ((*hhp - *zbhp) * 0.00004);
		}
		else
		{
			*xz = (*xz + *zspeed) + ((*hhp - *zbhp) * 0.00004);
		}
		if (*y - *yz < 0)//判断僵尸和kk的位置进行y轴位移操作
		{
			*yz = (*yz - *zspeed) - ((*hhp - *zbhp) * 0.00004);
		}
		else
		{
			*yz = (*yz + *zspeed) + ((*hhp - *zbhp) * 0.00004);
		}
	}
	if (fabs(*x - *xz) < a && fabs(*y - *yz) < a && *zbhp > 0)//kk靠近僵尸时的受伤判定
	{
		PlaySound(MAKEINTRESOURCE(IDR_WAVE4), NULL, SND_RESOURCE | SND_ASYNC | SND_NOSTOP);
		if (*x > *xz)  //kk受伤击退
			*x += 100;
		else
			*x -= 100;
		if (*x < 0)
			*x = 0;
		if (*x > 1200)//防止kk被击退出边界
			*x = 1200;
		*hp = *hp - 1;//hp减少
	}


	return *xz, * yz;
}
int dmyd(double* xd, double* yd, double* dspeed, IMAGE* hen, int* danmu, double* n, int* hp, double* x, double* y, int* henhen)//弹幕发射逻辑
{
	*n = *n + *dspeed;//弹幕位置累加
	if (henhen[0] == 1)
	{

		putimage(*xd + *n, *yd, &hen[0], SRCAND);//向左发射弹幕
		putimage(*xd + *n, *yd, &hen[1], SRCINVERT);
		if (fabs(*xd + *n - *x) < 30 && fabs(*yd - *y) < 30 && henhen[0] == 1)
		{
			PlaySound(MAKEINTRESOURCE(IDR_WAVE4), NULL, SND_RESOURCE | SND_ASYNC | SND_NOSTOP);
			*hp = *hp - 1;
			henhen[0] = 0;
		}
		if (*xd + *n > 1200 || *xd + *n < 0 || *yd>800 || *yd < 0)
			henhen[0] = 0;
	}

	if (henhen[1] == 1)
	{
		putimage(*xd - *n, *yd, &hen[0], SRCAND);//向右发射弹幕
		putimage(*xd - *n, *yd, &hen[1], SRCINVERT);
		if (fabs(*xd - *n - *x) < 30 && fabs(*yd - *y) < 30 && henhen[1] == 1)
		{
			PlaySound(MAKEINTRESOURCE(IDR_WAVE4), NULL, SND_RESOURCE | SND_ASYNC | SND_NOSTOP);
			*hp = *hp - 1;
			henhen[1] = 0;
		}
		if (*xd - *n > 1200 || *xd - *n < 0 || *yd>800 || *yd < 0)
			henhen[1] = 0;
	}

	if (henhen[2] == 1)
	{
		putimage(*xd, *yd + *n, &hen[0], SRCAND);//向下发射弹幕
		putimage(*xd, *yd + *n, &hen[1], SRCINVERT);
		if (fabs(*xd - *x) < 30 && fabs(*yd + *n - *y) < 30 && henhen[2] == 1)
		{
			PlaySound(MAKEINTRESOURCE(IDR_WAVE4), NULL, SND_RESOURCE | SND_ASYNC | SND_NOSTOP);
			*hp = *hp - 1;
			henhen[2] = 0;
		}
		if (*xd > 1200 || *xd < 0 || *yd + *n>800 || *yd + *n < 0)
			henhen[2] = 0;
	}

	if (henhen[3] == 1)
	{
		putimage(*xd, *yd - *n, &hen[0], SRCAND);//向上发射弹幕
		putimage(*xd, *yd - *n, &hen[1], SRCINVERT);
		if (fabs(*xd - *x) < 30 && fabs(*yd - *n - *y) < 30 && henhen[3] == 1)
		{
			PlaySound(MAKEINTRESOURCE(IDR_WAVE4), NULL, SND_RESOURCE | SND_ASYNC | SND_NOSTOP);
			*hp = *hp - 1;
			henhen[3] = 0;
		}
		if (*xd > 1200 || *xd < 0 || *yd - *n>800 || *yd - *n < 0)
			henhen[3] = 0;
	}

	if (henhen[4] == 1)
	{
		putimage(*xd + *n / sqrt(2), *yd - *n / sqrt(2), &hen[0], SRCAND);//向右上发射弹幕
		putimage(*xd + *n / sqrt(2), *yd - *n / sqrt(2), &hen[1], SRCINVERT);
		if (fabs(*xd + *n / sqrt(2) - *x) < 30 && fabs(*yd - *n - *y) < 30 && henhen[4] == 1)
		{
			PlaySound(MAKEINTRESOURCE(IDR_WAVE4), NULL, SND_RESOURCE | SND_ASYNC | SND_NOSTOP);
			*hp = *hp - 1;
			henhen[4] = 0;
		}
		if (*xd + *n / sqrt(2) > 1200 || *xd + *n / sqrt(2) < 0 || *yd - *n / sqrt(2) > 800 || *yd - *n / sqrt(2) < 0)
			henhen[4] = 0;
	}

	if (henhen[5] == 1)
	{
		putimage(*xd - *n / sqrt(2), *yd + *n / sqrt(2), &hen[0], SRCAND);//向左下发射弹幕
		putimage(*xd - *n / sqrt(2), *yd + *n / sqrt(2), &hen[1], SRCINVERT);
		if (fabs(*xd - *n / sqrt(2) - *x) < 30 && fabs(*yd + *n - *y) < 30 && henhen[5] == 1)
		{
			PlaySound(MAKEINTRESOURCE(IDR_WAVE4), NULL, SND_RESOURCE | SND_ASYNC | SND_NOSTOP);
			*hp = *hp - 1;
			henhen[5] = 0;
		}
		if (*xd - *n / sqrt(2) > 1200 || *xd - *n / sqrt(2) < 0 || *yd + *n / sqrt(2) > 800 || *yd + *n / sqrt(2) < 0)
			henhen[5] = 0;
	}


	if (henhen[6] == 1)
	{
		putimage(*xd - *n / sqrt(2), *yd - *n / sqrt(2), &hen[0], SRCAND);//向左上发射弹幕
		putimage(*xd - *n / sqrt(2), *yd - *n / sqrt(2), &hen[1], SRCINVERT);
		if (fabs(*xd - *n / sqrt(2) - *x) < 30 && fabs(*yd - *n - *y) < 30 && henhen[6] == 1)
		{
			PlaySound(MAKEINTRESOURCE(IDR_WAVE4), NULL, SND_RESOURCE | SND_ASYNC | SND_NOSTOP);
			*hp = *hp - 1;
			henhen[6] = 0;
		}
		if (*xd - *n / sqrt(2) > 1200 || *xd - *n / sqrt(2) < 0 || *yd - *n / sqrt(2) > 800 || *yd - *n / sqrt(2) < 0)
			henhen[6] = 0;
	}
	if (henhen[7] == 1)
	{
		putimage(*xd + *n / sqrt(2), *yd + *n / sqrt(2), &hen[0], SRCAND);//向右下发射弹幕
		putimage(*xd + *n / sqrt(2), *yd + *n / sqrt(2), &hen[1], SRCINVERT);
		if (fabs(*xd + *n / sqrt(2) - *x) < 30 && fabs(*yd + *n - *y) < 30 && henhen[7] == 1)
		{
			PlaySound(MAKEINTRESOURCE(IDR_WAVE4), NULL, SND_RESOURCE | SND_ASYNC | SND_NOSTOP);
			*hp = *hp - 1;
			henhen[7] = 0;
		}
		if (*xd + *n / sqrt(2) > 1200 || *xd + *n / sqrt(2) < 0 || *yd + *n / sqrt(2) > 800 || *yd + *n / sqrt(2) < 0)
			henhen[7] = 0;

	}


	return 0;
}
int fbf(int* hp, double* x, double* y, IMAGE* fb, int* ry, double* n, double* dspeed, int* fbm)
{
	if (*fbm == 1)
	{
		*n = *n + 2 * *dspeed;
		putimage(1200 - *n, *ry, &fb[0], SRCAND);
		putimage(1200 - *n, *ry, &fb[1], SRCINVERT);
		if (fabs(1200 - *n - *x) < 30 && fabs(*ry - *y) < 30)
		{
			PlaySound(MAKEINTRESOURCE(IDR_WAVE4), NULL, SND_RESOURCE | SND_ASYNC | SND_NOSTOP);
			*hp = *hp - 1;
			*fbm = 0;
			*n = 0;
			*ry = rand() % 750;
		}
		if (1200 - *n < 0)
		{
			*fbm = 0;
			*n = 0;
			*ry = rand() % 750;
		}

	}
	return 0;
}
int wbf(int* hp, double* x, double* y, IMAGE* wb, int* wy, double* m, double* dspeed, int* wbm)
{
	if (*wbm == 1)
	{
		*m = *m + 2 * *dspeed;
		putimage(1200 - *m, *wy, &wb[0], SRCAND);
		putimage(1200 - *m, *wy, &wb[1], SRCINVERT);
		if (fabs(1200 - *m - *x) < 30 && fabs(*wy - *y) < 30)
		{
			PlaySound(MAKEINTRESOURCE(IDR_WAVE4), NULL, SND_RESOURCE | SND_ASYNC | SND_NOSTOP);
			*hp = *hp - 1;
			*wbm = 0;
			*m = 0;
			*wy = rand() % 750;
		}
		if (1200 - *m < 0)
		{
			*wbm = 0;
			*m = 0;
			*wy = rand() % 750;
		}

	}
	return 0;
}
//篮球逻辑
int balllc(int* tools, int* bp, double* bkp, int* bb, double* xz, double* yz, char* bks, int* bkp1, int* bkp2, char* keyinput, char* xposition, IMAGE* ball, double* x, double* y, double* speed, int* zbhp, double* k, double* xd, double* yd, IMAGE* hen, double* dspeed, int* danmu, double xd1[], double yd1[], double* xz1, double* yz1, double* n, int* henhen, int p, int* MAGICdamage)
{
	
	if (*tools == 1)
	{
		if ((*keyinput == 'j' || *keyinput == 'J') && *bp != 1)//判断是否发射篮球
		{
			PlaySound(MAKEINTRESOURCE(IDR_WAVE1), NULL, SND_RESOURCE | SND_ASYNC);
			*bp = 1;
			*bkp1 = *x;
			*bkp2 = *y;
			*bks = *xposition;
			*bkp = 0;
		}

		if (*bp == 1 && *bks == 'd')//篮球向右发射条件
		{

			putimage(*bkp1 + *bkp, *bkp2, &ball[0], SRCAND); //加载篮球贴图同时进行篮球运动的计算
			putimage(*bkp1 + *bkp, *bkp2, &ball[1], SRCINVERT);
		}
		if (*bp == 1 && *bks == 'a')//；篮球向左发射条件
		{
			putimage(*bkp1 - *bkp, *bkp2, &ball[2], SRCAND); //加载篮球贴图同时进行篮球运动的计算
			putimage(*bkp1 - *bkp, *bkp2, &ball[3], SRCINVERT);
		}
		if ((*bks == 'd' && *bkp1 + *bkp > 1200) || (*bks == 'a' && *bkp1 - *bkp < 0))//篮球出界判定
		{
			*bp = 0;
			*bkp = 0;
		}
		if ((*bp == 1 && *bks == 'd' && fabs(*bkp1 + *bkp - *xz) < 30 && fabs(*bkp2 - *yz) < 30) || (*bp == 1 && *bks == 'a' && (fabs(*bkp1 - *bkp - *xz) < 30) && (fabs(*bkp2 - *yz) < 30)))//篮球击中判定
		{
			if (*zbhp > 0)//僵尸血量不为0执行
			{
				PlaySound(MAKEINTRESOURCE(IDR_WAVE2), NULL, SND_RESOURCE | SND_ASYNC);
				*bp = 0;
				*bkp = 0;
				*zbhp = *zbhp - *MAGICdamage;
				if (rand() % 10000 + 1 < (10000 * *k))//弹幕概率概率公式
				{
					*danmu = *danmu + 1;//触发弹幕

					if (henhen[0] == 0 && henhen[1] == 0 && henhen[2] == 0 && henhen[3] == 0 && henhen[4] == 0 && henhen[5] == 0 && henhen[6] == 0 && henhen[7] == 0)
					{
						*n = 0;//清零累加坐标重新发射
						*xd = *xz;//弹幕x轴坐标赋值
						*yd = *yz;//弹幕y轴坐标赋值
						for (p = 0; p < 8; p++)
							henhen[p] = 1;

					}


				}
			}
		}
	}
	if (*tools == 2)
	{
		;
	}
	return 0;
}

//加载僵尸
int putzb(int* zbhp, double* x, double* xz, double* yz, IMAGE* zb, double* num_time, int* door, IMAGE* dragon)
{
	if (*zbhp > 0)//判断是否加载僵尸
	{
		if (*x < *xz)//僵尸运动方向判定
		{
			if (*door < 3)
			{
				putimage(*xz, *yz, &zb[0], SRCAND);
				putimage(*xz, *yz, &zb[1], SRCINVERT);
			}
			if (*door == 3)
			{
				putimage(*xz, *yz, &dragon[0], SRCAND);
				putimage(*xz, *yz, &dragon[1], SRCINVERT);
			}
		}
		else
		{
			if (*door < 3)
			{
				putimage(*xz, *yz, &zb[2], SRCAND);
				putimage(*xz, *yz, &zb[3], SRCINVERT);
			}
			if (*door == 3)
			{
				putimage(*xz, *yz, &dragon[2], SRCAND);
				putimage(*xz, *yz, &dragon[3], SRCINVERT);
			}

		}
	}
	return 0;
}


int main()
{
	MOUSEMSG position_test;
	hwnd = initgraph(1200, 800);
	SetWindowLongPtr(hwnd, GWL_STYLE, GetWindowLongPtr(hwnd, GWL_STYLE) | WS_THICKFRAME);
	SetWindowPos(hwnd, NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
	MainHdc = GetDC(hwnd);
	setbkcolor(WHITE);
	  //kunkun 的 初始坐标 
	loadimage(&img, ".\\photo\\kunkun.jpg", 1200, 800);  //开场贴图
	
	loadimage(&door3, ".\\photo\\door3.jpg");//第四关

	loadimage(&jch, ".\\photo\\jch.jpg");  //教程贴图
	IMAGE shadows_1;
	loadimage(&shadows_1, ".\\photo\\shadows_1.jpg");//隐藏关
	IMAGE kk[4];    //kunkun 的贴图

	//kunkun 的贴图

	loadimage(&kk[0], ".\\photo\\11.jpg", 100, 100);
	loadimage(&kk[1], ".\\photo\\22.jpg", 100, 100);
	loadimage(&kk[2], ".\\photo\\44.jpg", 100, 100);
	loadimage(&kk[3], ".\\photo\\33.jpg", 100, 100);

	loadimage(&ball[0], ".\\photo\\ball1.jpg", 80, 80);
	loadimage(&ball[1], ".\\photo\\ball2.jpg", 80, 80);
	loadimage(&ball[2], ".\\photo\\ball3.jpg", 80, 80);
	loadimage(&ball[3], ".\\photo\\ball4.jpg", 80, 80);

	loadimage(&bk, ".\\photo\\bk.jpg");
	loadimage(&bk2, ".\\photo\\bk2.jpg");

	loadimage(&bk3, ".\\photo\\stage_2.jpg");

	loadimage(&zb[0], ".\\photo\\zombie1.jpg", 90, 90);
	loadimage(&zb[1], ".\\photo\\zombie2.jpg", 90, 90);
	loadimage(&zb[2], ".\\photo\\zombie3.jpg", 90, 90);
	loadimage(&zb[3], ".\\photo\\zombie4.jpg", 90, 90);

	loadimage(&pause[0], ".\\photo\\pause_menu_1.jpg");
	loadimage(&pause[1], ".\\photo\\pause_menu_2.jpg");

	loadimage(&key[0], ".\\photo\\key2.jpg", 50, 50);
	loadimage(&key[1], ".\\photo\\key1.jpg", 50, 50);

	loadimage(&ys[0], ".\\photo\\c29.jpg", 50, 50);
	loadimage(&ys[1], ".\\photo\\c28.jpg", 50, 50);


	loadimage(&hen[0], ".\\photo\\hen1.jpg", 80, 80);
	loadimage(&hen[1], ".\\photo\\hen2.jpg", 80, 80);

	loadimage(&die_1, ".\\photo\\213.jpg", 0, 0);


	loadimage(&kuwu[0], ".\\photo\\kuwu2.jpg", 100, 100);
	loadimage(&kuwu[1], ".\\photo\\kuwu1.jpg", 100, 100);


	loadimage(&dragon[0], ".\\photo\\long2.jpg", 200, 200);
	loadimage(&dragon[1], ".\\photo\\long1.jpg", 200, 200);
	loadimage(&dragon[2], ".\\photo\\long4.jpg", 200, 200);
	loadimage(&dragon[3], ".\\photo\\long3.jpg", 200, 200);

	loadimage(&fb[0], ".\\photo\\fb.jpg", 100, 100);
	loadimage(&fb[1], ".\\photo\\fb1.jpg", 100, 100);


	loadimage(&wb[0], ".\\photo\\wb2.jpg", 100, 100);
	loadimage(&wb[1], ".\\photo\\wb1.jpg", 100, 100);


	loadimage(&arrow[0], ".\\photo\\w1.jpg", 100, 100);
	loadimage(&arrow[1], ".\\photo\\w2.jpg", 100, 100);
	loadimage(&arrow[2], ".\\photo\\s1.jpg", 100, 100);
	loadimage(&arrow[3], ".\\photo\\s2.jpg", 100, 100);
	loadimage(&arrow[4], ".\\photo\\a1.jpg", 100, 100);
	loadimage(&arrow[5], ".\\photo\\a2.jpg", 100, 100);
	loadimage(&arrow[6], ".\\photo\\d1.jpg", 100, 100);
	loadimage(&arrow[7], ".\\photo\\d2.jpg", 100, 100);
	int i = 1;
	
	loadimage(&GEN[1], ".\\photo\\gen1.jpg", 100, 100);
	loadimage(&GEN[0], ".\\photo\\gen2.jpg", 100, 100);
	loadimage(&GEN[3], ".\\photo\\gen3.jpg", 130, 130);
	loadimage(&GEN[2], ".\\photo\\gen4.jpg", 130, 130);
	loadimage(&GEN[5], ".\\photo\\gen5.jpg", 130, 130);
	loadimage(&GEN[4], ".\\photo\\gen6.jpg", 130, 130);

	IMAGE rotate_weapon[2];//旋转武器的暂时变量

	IMAGE box[20];
	loadimage(&box[0], ".\\photo\\box2.jpg", 100, 100);
	loadimage(&box[1], ".\\photo\\box1.jpg", 100, 100);
	loadimage(&box[2], ".\\photo\\box4.jpg", 100, 100);
	loadimage(&box[3], ".\\photo\\box3.jpg", 100, 100);


	rotateimage(&box[4], &box[2], PI / 6, WHITE, true, true);
	rotateimage(&box[5], &box[3], PI / 6, BLACK, true, true);

	double SwordRotateAngel = PI / 12;

	
	int open_inventory = 0;  //判断是否打开物品栏

	char  button = 'w';

	int danmu = -1;//弹幕是否发射参数

	double n, m = 0;//弹幕累加归零参数

	double xd, yd;//弹幕坐标参数

	double xd1[30] = { 0 }, yd1[30] = { 0 };//待用参数

	int ix = 77, iy = 342;//物品栏的选择初始格子
	int wp[45] = { 0 };//物品栏的储存数组
	int flash_wpn = 0;
	int wpn = 0;
	int bp = 0;//   򼤻     
	double bkp = 0;//       
	int bb = 0;//         
	char bks;//        
	int bkp1, bkp2;//      
	char xposition = 'd';
	char keyinput = 0;
	int door = 0;
	int yss = 0;
	int kux = 0, kuy = 0, ku = 0;
	int hilishin = 0;
	int fbm = 0, wbm = 0;
	srand((unsigned int)time(NULL));//取随机数
	int zbhp1_1 = 0;
	int zbhp2_1 = 0;
	int zbhp3_1 = 0;

	int rb, rq = 0;
	double xz, yz, xz1 = 1000, yz1 = 390, xz2 = 1100, yz2, xz3 = 1100, yz3 = 390;

	yz2 = rand() % 601 + 100;//僵尸随机出现的坐标

	double k;

	double xxz = 0, yyz = 0;

	int hp = 3, hhp = 1000;
	int strength = 2;
	int quickness = 2;
	int intelligence = 2;
	int MAGICdamage;
	int phycaldamage;

	int zbhp, zbhp1 = 1000, zbhp2 = 200, zbhp3 = 1000;

	double zspeed = 0.1, speed = 0.95, cspeed = 0.3, dspeed = 0.5;

	int ry, wy = rand() % 800 + 1;
	int henhen[8] = { 0 };//僵尸弹幕的激活状态
	int p = 0;
	int gain_key = 0;
	int tools = 1;
	int able_to_attack = 0 ; int rotate_count = 0; //able_to_attack判断是否可以攻击，防止动画重复,rotate_count计算旋转次数
	int nx = 0;//循环变量
	int lv = 1;//等级
	int ex = 0;//经验
	int aex = 0;
	int bex = 0;

	struct OLDRECT {
		int right;
		int bottom;
	};

	OLDRECT oldrect;
	bool Start = true;
	double angle = 0;
	TCHAR cxk_health[20];
	TCHAR cxk_health_num[20];
	TCHAR game_lost[20];
	TCHAR genshin[100];
	TCHAR mouse_position[100];

	PlaySound(MAKEINTRESOURCE(IDR_WAVE15), NULL, SND_RESOURCE | SND_ASYNC | SND_NOSTOP);
	BeginBatchDraw();
	GetClientRect(hwnd, &rect);
	Putimage(0, 0, rect.right, rect.bottom, &img);
	while (1)
	{
		
		oldrect.right = rect.right;
		oldrect.bottom = rect.bottom;
		GetClientRect(hwnd, &rect);
		kwidth = rect.right / 1200.0;
		khight = rect.bottom / 800.0;
		if (GetAsyncKeyState(VK_F11) & 0x8000) {
			ToggleFullScreen();
			Sleep(200);
		}
		if (oldrect.right != rect.right || oldrect.bottom != rect.bottom) {
			sizechange = true;
		}
		if (Start) {
			if (sizechange)
			{
				sizechange = false;
				Putimage(0, 0, rect.right, rect.bottom, &img);
			}
			if (_kbhit())
			{
				_getch();
				Start = false;
				sizechange = true;
			}
		}
		else {
			if (sizechange)
			{
				Putimage(0, 0, rect.right, rect.bottom, &jch);
			}
			if ((GetAsyncKeyState(0x57) & 0x8000 && button != 'w') || (button == 'w' && sizechange)) {
				Putimage(0, 0, rect.right, rect.bottom, &jch);
				button = 'w';
				Putimage(550 * kwidth, 450 * khight, kwidth * arrow[0].getwidth(), khight * arrow[0].getheight(), &arrow[0], SRCAND);
				Putimage(550 * kwidth, 450 * khight, kwidth * arrow[1].getwidth(), khight * arrow[1].getheight(), &arrow[1], SRCINVERT);
			}
			                 //判断是否移动 W
			if ((GetAsyncKeyState(0x53) & 0x8000 && button != 's') || (button == 's' && sizechange)) {//判断是否移动 S 
				Putimage(0, 0, rect.right, rect.bottom, &jch);
				button = 's';
				Putimage(550 * kwidth, 470 * khight, kwidth * arrow[2].getwidth(), khight * arrow[2].getheight(), &arrow[2], SRCAND);
				Putimage(550 * kwidth, 470 * khight, kwidth * arrow[3].getwidth(), khight * arrow[3].getheight(), &arrow[3], SRCINVERT);
			}
			if ((GetAsyncKeyState(0x41) & 0x8000 && button != 'a' ) || (button == 'a' && sizechange)) {//判断是否移动 A
				Putimage(0, 0, rect.right, rect.bottom, &jch);
				button = 'a';
				Putimage(550 * kwidth, 470 * khight, kwidth * arrow[4].getwidth(), khight * arrow[4].getheight(), &arrow[4], SRCAND);
				Putimage(550 * kwidth, 470 * khight, kwidth * arrow[5].getwidth(), khight * arrow[5].getheight(), &arrow[5], SRCINVERT);
			}

			if ((GetAsyncKeyState(0x44) & 0x8000 && button != 'd' ) || (button == 'd' && sizechange)) {//判断是否移动 D
				Putimage(0, 0, rect.right, rect.bottom, &jch);
				button = 'd';
				Putimage(550 * kwidth, 470 * khight, kwidth * arrow[6].getwidth(), khight * arrow[6].getheight(), &arrow[6], SRCAND);
				Putimage(550 * kwidth, 470 * khight, kwidth * arrow[7].getwidth(), khight * arrow[7].getheight(), &arrow[7], SRCINVERT);
				
			}
			if (_kbhit())
			{
				_getch();

				if (GetAsyncKeyState(0x46) & 0x8000)
				{
					break;
				}
			}
			sizechange = false;

		}
		
	}
	PlaySound(NULL, NULL, SND_FILENAME);
	clock_t start_time, end_time;//记录开始时间和结束时间
	double num_time = 0;//计时器
	switch (button)
	{
	case 'w': {
		hp = 1;
		break;
	}
	case 's':
	{
		hp = 5;
		zbhp1 = 200;
		zbhp3 = 200;
		break;
	}
	case 'a':
	{
		hp = 2;
		break;
	}
	case'd':
	{
		hp = 3;
	}
	}


	while (1)
	{
		GetClientRect(hwnd, &rect);
		kwidth = rect.right / 1200.0;
		khight = rect.bottom / 800.0;
		if (intelligence <= 5)
			MAGICdamage = 20 + intelligence * 15;
		if (5 < intelligence && intelligence <= 10)
			MAGICdamage = 20 + 5 * 15 + (intelligence - 5) * 10;
		if (10 < intelligence)
			MAGICdamage = 20 + 5 * 15 + 5 * 10 + (intelligence - 10) * 5;
		phycaldamage = quickness + strength;
		if (ex >= 500)
		{
			for (nx = 1; nx < 100; nx++)
			{
				if (500 * nx * (1 + nx) / 2 >= ex)
				{

					lv = nx;
					aex = 500 * (nx - 1);
					bex = ex - 500 * nx * (nx - 1);
					break;
				}
			}
		}
		bkp += speed;
		start_time = clock();//记录开始时间
		angle += PI / 1800;
		k = -(0.9 / 1520) * zbhp1 * zbhp1 + (1 - (0.9 / 1520));//弹幕触发概率与僵尸血量的函数关系,传递k变量

		_stprintf_s(cxk_health, _T("剩余生命值:"));
		_stprintf_s(cxk_health_num, _T(" %d"), hp);
		_stprintf_s(genshin, _T("你获得了原石之中蕴藏的查克拉，你感觉身体更强壮了并且学会如何在水上行走。"));

		if (_kbhit())
			keyinput = _getch();
		if (door == 2)
			putimage(0, 0, &bk3);
		if (door == 1)
			putimage(0, 0, &bk2);
		if (door == 0)
			putimage(0, 0, &bk);
		if (door == 3)
			putimage(0, 0, &door3);

		if (door == -1)
			putimage(0, 0, &shadows_1);
	




		putimage(400, 650, &box[2], SRCAND);//宝箱
		putimage(400, 650, &box[3], SRCINVERT);
		int abl = 1.414 * 100 * 0.500000 / sin(7 / 12 * PI);
		putimage(500, 650-abl, &box[4], SRCAND);//宝箱
		putimage(500, 650-abl, &box[5], SRCINVERT);

		if (GetAsyncKeyState(VK_F11) & 0x8000 ) {
			ToggleFullScreen();
			Sleep(200);
		}


		while(open_inventory == 1) {

			
			inventory(wp, &open_inventory, &ix, &iy, &strength, &quickness, &intelligence, &lv, &itemchange); 

		}


		if (zbhp1 > 0)
		{
			zbhp = zbhp1;//传递僵尸1的参数给僵尸生成函数
			xz = xz1;
			yz = yz1;

			putzb(&zbhp, &x, &xz, &yz, zb, &num_time, &door, dragon);
			zbhp = zbhp1;//传递僵尸1的参数给ballc函数判断球是否击中僵尸和判定是否发射弹幕
			xz = xz1;
			yz = yz1;

			balllc(&tools, &bp, &bkp, &bb, &xz, &yz, &bks, &bkp1, &bkp2, &keyinput, &xposition, ball, &x, &y, &speed, &zbhp, &k, &xd, &yd, hen, &dspeed, &danmu, yd1, xd1, &xz1, &yz1, &n, henhen, p, &MAGICdamage);

			zbhp1 = zbhp;//传递僵尸1的参数给zblc函数进行僵尸移动的处理
			xz = xz1;
			yz = yz1; 

			zblc(&xz, &yz, &x, &y, &hp, &zspeed, &hhp, &zbhp, &door);
			xz1 = xz;//传出处理后的参数返回给僵尸1
			yz1 = yz;
			zbhp1 = zbhp;

			if (danmu > -1)//判定弹幕是否射出
			{
				xd = xz;//将射中僵尸的坐标参数传给弹幕坐标
				yd = yz;
				dmyd(&xd, &yd, &dspeed, hen, &danmu, &n, &hp, &x, &y, henhen);
			}
		}

		if (num_time > 60 && door < 3)//计时大于60s时执行
		{
			if (zbhp2 > 0)
			{
				if (zbhp1 > 0)
				{
					n = n - dspeed;//修复第二只僵尸出现时弹幕速度异常
				}

				zbhp = zbhp2;//将第二个僵尸的参数传给僵尸生成函数
				xz = xz2;
				yz = yz2;
				putzb(&zbhp, &x, &xz, &yz, zb, &num_time, &door, dragon);


				zbhp = zbhp2;//传递僵尸2的参数给ballc函数判断球是否击中僵尸和判定是否发射弹幕
				xz = xz2;
				yz = yz2;

				balllc(&tools, &bp, &bkp, &bb, &xz, &yz, &bks, &bkp1, &bkp2, &keyinput, &xposition, ball, &x, &y, &speed, &zbhp, &k, &xd, &yd, hen, &dspeed, &danmu, xd1, yd1, &xz1, &yz1, &n, henhen, p, &MAGICdamage);

				zbhp2 = zbhp;//传递僵尸2的参数给zblc函数进行僵尸移动的处理
				xz = xz2;
				yz = yz2;

				zblc(&xz, &yz, &x, &y, &hp, &zspeed, &hhp, &zbhp, &door);
				xz2 = xz;//传出处理后的参数返回给僵尸2
				yz2 = yz;
				zbhp2 = zbhp;
			}

			if (danmu > -1)//判定弹幕是否射出
			{
				if (zbhp2 > zbhp1 && zbhp1 == 0)
				{
					n = 0;
					zbhp1--;

				}
				xd = xz;//将射中僵尸的坐标参数传给弹幕坐标
				yd = yz;

				//dmyd(&xd, &yd, &dspeed, hen, &danmu, &n, &hp, &x, &y, henhen);

			}
		}
		if (zbhp3 > 0 && door == 3)
		{
			zbhp = zbhp3;//传递僵尸3的参数给僵尸生成函数
			xz = xz3;
			yz = yz3;

			putzb(&zbhp, &x, &xz, &yz, zb, &num_time, &door, dragon);
			rb = rand() % 2;
			rq = rand() % 2;
			if (rb == 0 && fbm == 0)
			{
				fbm = 1;
			}
			fbf(&hp, &x, &y, fb, &ry, &n, &dspeed, &fbm);
			if (rq == 0 && wbm == 0)
			{
				wbm = 1;
			}
			wbf(&hp, &x, &y, wb, &wy, &m, &dspeed, &wbm);

			zbhp = zbhp3;//传递僵尸3的参数给ballc函数判断球是否击中僵尸和判定是否发射弹幕
			xz = xz3;
			yz = yz3;
			balllc(&tools, &bp, &bkp, &bb, &xz, &yz, &bks, &bkp1, &bkp2, &keyinput, &xposition, ball, &x, &y, &speed, &zbhp, &k, &xd, &yd, hen, &dspeed, &danmu, yd1, xd1, &xz3, &yz3, &n, henhen, p, &MAGICdamage);
			zbhp3 = zbhp;//传递僵尸3的参数给zblc函数进行僵尸移动的处理
			xz = xz3;
			yz = yz3;
			zblc(&xz, &yz, &x, &y, &hp, &zspeed, &hhp, &zbhp, &door);
			xz3 = xz;//传出处理后的参数返回给僵尸3
			yz3 = yz;
			zbhp3 = zbhp;

		}
		if (keyinput == 'g')
		{

		}
		if (xposition == 'a' )//判断kk的移动方向
		{
			if (able_to_attack == 0) 
			{
				putimage(x, y - 20, &GEN[4], SRCAND);
				putimage(x, y - 20, &GEN[5], SRCINVERT);
			}
			
			putimage(x, y, &kk[2], SRCAND);//加载kk的移动方向贴图
			putimage(x, y, &kk[3], SRCINVERT);

		}
		else if(xposition == 'd' )
		{

			if (able_to_attack == 0)
			{
				putimage(x - 30, y - 20, &GEN[2], SRCAND);
				putimage(x - 30, y - 20, &GEN[3], SRCINVERT);
			}
			
			putimage(x, y, &kk[0], SRCAND);//加载kk的移动方向贴图
			putimage(x, y, &kk[1], SRCINVERT);

		}

		

		if ( (GetAsyncKeyState(0x45) & 0x8000 || GetAsyncKeyState(0x65) & 0x8000) && able_to_attack == 0 )//判断是否 E
		{
			rotate_count = 0;
			able_to_attack = 1;
		}

		SwordWeaponAttact(xposition, x, y, GEN , rotate_weapon,&rotate_count, &able_to_attack , SwordRotateAngel);
		

		if (GetAsyncKeyState(0x57) & 0x8000)//判断是否移动 W
		{
			y -= cspeed;
			if (y < 0)
				y += cspeed;//边界限定


		}
		if (GetAsyncKeyState(0x53) & 0x8000)//判断是否移动 S 
		{
			y += cspeed;
			if (y > 700)
				y -= cspeed;//边界限定
		}
		if (GetAsyncKeyState(0x41) & 0x8000)//判断是否移动 A
		{
			x -= cspeed;
			if (x < 0)
				x += cspeed;//边界限定
			xposition = 'a';
		}
		if (GetAsyncKeyState(0x44) & 0x8000)//判断是否移动 D
		{
			x += cspeed;
			if (x > 1100)
				x -= cspeed;//边界限定
			xposition = 'd';
		}
		if (GetAsyncKeyState(0x50) & 0x8000 || GetAsyncKeyState(0x80) & 0x8000) //游戏暂停
		{

			FlushBatchDraw();
			putimage(0, 0, &pause[0], SRCAND);
			putimage(0, 0, &pause[1], SRCINVERT);

			FlushBatchDraw();
			system("pause");
		}
		setbkmode(TRANSPARENT);//显示cxk血量四个字
		settextcolor(WHITE);
		settextstyle(50, 0, _T("黑体"));
		outtextxy(1, 0, cxk_health);

		setbkmode(TRANSPARENT);//显示cxk血量的具体数字
		settextcolor(YELLOW);
		settextstyle(50, 0, _T("黑体"));
		outtextxy(245, 0, cxk_health_num);
		if (GetAsyncKeyState(VK_LBUTTON))
		{
			position_test = GetMouseMsg();
			_stprintf_s(mouse_position, _T("[ %d , %d ]"), position_test.x, position_test.y);
			setbkmode(TRANSPARENT);
			settextcolor(WHITE);
			settextstyle(50, 0, _T("黑体"));
			outtextxy(524, 417, mouse_position);
		}


		if (GetAsyncKeyState(0x49) || GetAsyncKeyState(0x69)) //打开物品栏
		{
			open_inventory = 1;
		    
		}


		keyset(&zbhp, &gain_key, &xz, &yz, key, &x, &y, &wpn, wp, &flash_wpn);


		while (flash_wpn == 1)  //刷新下一个物品拾取格
		{
			for (int i = 0; i < 45; i++)
			{
				if (wp[i] == 0) {  //从0开始遍历，看哪一个物品格是空的，就给他指向那个位置，比如 1 0 2 0，那么就指向第二格的0而不是简单递增的第四格子的0
					wpn = i;
					flash_wpn = 0;
					break;
				}
			}

		}

		if (hp <= 0)//kk血量为0时结束游戏
		{

			break;

		}
		if (door == 1 && yss == 0)//原石显示
		{
			putimage(580, 200, &ys[0], SRCAND);
			putimage(580, 200, &ys[1], SRCINVERT);
		}
		if (fabs(x - 580) < 50 && fabs(y - 200) < 50 && door == 1 && yss == 0)//原石拾取
		{
			PlaySound(MAKEINTRESOURCE(IDR_WAVE5), NULL, SND_RESOURCE | SND_ASYNC | SND_NOSTOP);
			yss = 1;
			if (button == 'w')
			{
				hp = 2;
			}
			if (button == 's')
			{
				hp = 10;
			}
			if (button == 'a')
			{
				hp = 4;
			}
			if (button == 'd')
			{
				hp = 5;
			}
			if (wpn < 45) {   //判断物品栏格子  
				wp[wpn] = 3;  //3 为原石 ID
				
			}
			flash_wpn = 1;
		}

		if (1)//经验获取
		{
			if (zbhp1 <= 0 && zbhp1_1 == 0)
			{
				ex += 1000;
				zbhp1_1 = 1;
			}
			if (zbhp2 <= 0 && zbhp2_1 == 0)
			{
				ex += 1000;
				zbhp2_1 = 1;
			}
			if (zbhp3 <= 0 && zbhp3_1 == 0)
			{
				ex += 3000;
				zbhp3_1 = 1;
			}
		}
		if (door == 1 && yss == 1)//显示获取原石的独白
		{
			setbkmode(TRANSPARENT);
			settextcolor(RED);
			settextstyle(25, 0, _T("黑体"));
			outtextxy(1, 600, genshin);
		}
		if (gain_key == 1 && y < 10 && x > 750 && x < 850 && door == 0)//进入第二关
		{
			y = 770;
			x = 550;
     		door = 1;

		}

		if (y < 10 && x > 500 && x < 650 && door == 1)//进入第三关
		{
			y = 600;
			x = 200;
			door = 2;
		}
		if (y < 290 && y>230 && x > 970 && x < 1200 && door == 2)//进入第四关
		{
			n = 0;
			ku = 0;
			y = 600;
			x = 200;
			door = 3;

		}
		if (door == 2 && y < 430 && y>360 && x <= 20)//进入隐藏关
		{
			door = -1;
			x = 1100;
			
		}

		if (door == -1)//隐藏关
		{
			if (x < 640 && y < 520)
			{
				x += cspeed;
				y += cspeed;
			}
			if (x > 280 && x < 520 && y>530 && y < 770)
			{

			}
			putimage(400, 650, &box[2], SRCAND);//宝箱
			putimage(400, 650, &box[3], SRCINVERT);

		}
		if (door == 1)//关卡2的边界
		{
			if (x < 480)
				x += cspeed;
			if (x > 630)
				x -= cspeed;

		}
		if (door == 2 && hilishin == 0)
		{
			putimage(600, 320, &kuwu[0], SRCAND);//飞雷神之术
			putimage(600, 320, &kuwu[1], SRCINVERT);
			if (fabs(x - 600) < 50 && fabs(y - 320) < 50)//飞雷神拾取
			{
				PlaySound(MAKEINTRESOURCE(IDR_WAVE5), NULL, SND_RESOURCE | SND_ASYNC | SND_NOSTOP);
				hilishin = 1;
				if (wpn < 45) {   //判断物品栏格子  
					wp[wpn] = 2;  //2 为苦无 ID
				}
				flash_wpn = 1;
			}
		}

		if (hilishin == 1)
		{
			if (ku == 1)
			{
				putimage(kux, kuy, &kuwu[0], SRCAND);//飞雷神之术
				putimage(kux, kuy, &kuwu[1], SRCINVERT);
			}
			if (keyinput == 'k' || keyinput == 'K')
			{
				if (ku == 1)
				{
					x = kux;
					y = kuy;
					ku = 0;
					kux = -100;
					kuy = -100;
				}
				else if (ku == 0 && kux != x)
				{
					kux = x;
					kuy = y;
					ku = 1;
				}
			}
		}
		FlushBatchDraw();
	
		keyinput = 0;
		end_time = clock();//记录结束时间ddd
		num_time += (double)(end_time - start_time) / CLOCKS_PER_SEC;//记录运行总时间
	}

	EndBatchDraw();

	putimage(0, 0, &die_1);
	PlaySound(NULL, NULL, SND_FILENAME);
	PlaySound(MAKEINTRESOURCE(IDR_WAVE13), NULL, SND_RESOURCE | SND_ASYNC | SND_NOSTOP);
	_getch();
	closegraph();

	return 0;
}