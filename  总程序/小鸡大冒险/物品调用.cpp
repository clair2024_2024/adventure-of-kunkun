﻿#include <stdio.h>
#include <graphics.h>	
#include <conio.h>
#include <time.h>
#include <math.h>
#include "resource.h"
#pragma comment(lib, "Winmm.lib") 


int item(int x, int y, int dsx, int dsy, int id) {

	if (id == 1) {
		IMAGE key[2];  //钥匙图层
		loadimage(&key[0], ".\\photo\\key2.jpg", dsx, dsy);
		loadimage(&key[1], ".\\photo\\key1.jpg", dsx, dsy);


		putimage(x, y, &key[0], SRCAND);
		putimage(x, y, &key[1], SRCINVERT);

	}
	if (id == 2) {

		IMAGE kuwu[2]; //苦无的贴图
		loadimage(&kuwu[0], ".\\photo\\kuwu2.jpg", dsx, dsy);
		loadimage(&kuwu[1], ".\\photo\\kuwu1.jpg", dsx, dsy);

		putimage(x, y, &kuwu[0], SRCAND);
		putimage(x, y, &kuwu[1], SRCINVERT);
	}
	if (id == 3) {

		IMAGE ys[2];  //原石 的 贴图
		loadimage(&ys[0], ".\\photo\\c29.jpg", dsx, dsy);
		loadimage(&ys[1], ".\\photo\\c28.jpg", dsx, dsy);

		putimage(x, y, &ys[0], SRCAND);
		putimage(x, y, &ys[1], SRCINVERT);

	}

	return 0;
}

int item_discribe(int id) {
	if (id == 1) {
		TCHAR  id01[20];
		_stprintf_s(id01, _T("篮球场钥匙"));
		setbkmode(TRANSPARENT);
		settextcolor(WHITE);
		settextstyle(50, 0, _T("黑体"));
		outtextxy(200, 220, id01);


		TCHAR  id01_describe_01[50];
		TCHAR  id01_describe_02[50];
		_stprintf_s(id01_describe_01, _T("描述: 能够拿来打开篮球场右上角的门"));
		_stprintf_s(id01_describe_02, _T("打开球场大门后这把钥匙便毫无用处了"));
		setbkmode(TRANSPARENT);
		settextcolor(WHITE);
		settextstyle(35, 0, _T("黑体"));
		outtextxy(570, 220, id01_describe_01);

		setbkmode(TRANSPARENT);
		settextcolor(RED);
		settextstyle(35, 0, _T("黑体"));
		outtextxy(570, 255, id01_describe_02);

	}
	if (id == 2) {
		TCHAR  id02[20];
		_stprintf_s(id02, _T("飞雷神苦无"));
		setbkmode(TRANSPARENT);
		settextcolor(WHITE);
		settextstyle(50, 0, _T("黑体"));
		outtextxy(200, 220, id02);

		TCHAR  id02_describe_1[50];
		TCHAR  id02_describe_2[50];
		TCHAR  id02_describe_3[50];
		TCHAR  id02_describe_4[50];
		TCHAR  id02_describe_5[50];
		_stprintf_s(id02_describe_1, _T("描述: 来自异世界的苦无"));
		_stprintf_s(id02_describe_2, _T("具有让人时空传送的能力"));
		_stprintf_s(id02_describe_3, _T("攻击力[小]"));
		_stprintf_s(id02_describe_4, _T("敏捷[中]"));
		_stprintf_s(id02_describe_5, _T("空间折跃"));
		setbkmode(TRANSPARENT);
		settextcolor(WHITE);
		settextstyle(35, 0, _T("黑体"));
		outtextxy(570, 220, id02_describe_1);
		outtextxy(570, 255, id02_describe_2);

		setbkmode(TRANSPARENT);
		settextcolor(RED);
		settextstyle(35, 0, _T("黑体"));
		outtextxy(570, 290, id02_describe_3);
		outtextxy(570, 325, id02_describe_4);
		outtextxy(570, 360, id02_describe_5);
	}
	if (id == 3)
	{
		TCHAR  id03[20];
		_stprintf_s(id03, _T("原石护身符"));
		setbkmode(TRANSPARENT);
		settextcolor(WHITE);
		settextstyle(50, 0, _T("黑体"));
		outtextxy(200, 220, id03);

		TCHAR  id03_describe_1[50];
		TCHAR  id03_describe_2[50];
		TCHAR  id03_describe_3[50];
		_stprintf_s(id03_describe_1, _T("描述: 原石形状的护身符"));
		_stprintf_s(id03_describe_2, _T("能够增强体魄且站立于水面"));
		setbkmode(TRANSPARENT);
		settextcolor(WHITE);
		settextstyle(35, 0, _T("黑体"));
		outtextxy(570, 220, id03_describe_1);
		outtextxy(570, 255, id03_describe_2);

		setbkmode(TRANSPARENT);
		settextcolor(RED);
		settextstyle(35, 0, _T("黑体"));
		_stprintf_s(id03_describe_3, _T("最大生命值提升[小]"));
		outtextxy(570, 290, id03_describe_3);

	}
	return 0;
}