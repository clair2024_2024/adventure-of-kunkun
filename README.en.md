# The Great Adventure of Kunkun

#### Description
In a mysterious and colorful world, there is a brave young man named Kun Kun. He has a heart full of curiosity and adventurous spirit, determined to embark on a journey full of unknowns and surprises. This is "Kun Kun's Great Adventure", a game that combines adventure, puzzle-solving, and role-playing, leading players into Kun Kun's fantastical world to explore endless mysteries together.

-- The above is all made up, but this is indeed an open-source game based on EasyX.

#### Installation

1.  Install the EazyX extension library on an IDE that supports EazyX

2.  Download the material file package and place it in the corresponding folder

3. Run the program

##### Potential Problems 

1. If you find that "File not found" , delete the "IDRWAVE14" may work out


#### Contributors

   ##### [克莱尔](https://gitee.com/clair2024_2024) [NoneWhite](https://gitee.com/nonewhite) [AmeoInoru](https://gitee.com/ameoinoru)


