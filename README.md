# The Great Adventure of Kunkun-小鸡大冒险

#### 介绍
在一个神秘而又多彩的世界中，有一位勇敢的少年——坤坤。他拥有一颗充满好奇和冒险精神的心，决心踏上一段充满未知与惊喜的旅程。这就是“坤坤大冒险”，一款集冒险、解谜、角色扮演于一体的游戏，带领玩家进入坤坤的奇幻世界，共同探索无尽的奥秘

### ——以上都是编的，但这确实是个基于EasyX的开源小游戏。

#### 安装教程

1.  在可支持EazyX的IDE上安装EazyX扩展库
2.  下载素材文件包并放入对应文件夹
3.  运行程序

#### 可能出现的问题

1.  提示 File Not Found时，删除IDRWAVE14的路径即可

#### 贡献者 ：
   ##### [克莱尔](https://gitee.com/clair2024_2024) [NoneWhite](https://gitee.com/nonewhite) [AmeoInoru](https://gitee.com/ameoinoru)


